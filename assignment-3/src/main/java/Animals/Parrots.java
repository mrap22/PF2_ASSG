package Animals;

import Species.Aves;
import javari.animal.Condition;
import javari.animal.Gender;

public class Parrots extends Aves {
    public Parrots(int id, String type, String name, Gender gender, double length,
            double weight, Condition condition, String scon) {
    super(id, type, name, gender, length, weight, condition, scon);
    }
}
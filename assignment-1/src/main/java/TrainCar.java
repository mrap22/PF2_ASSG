public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    private WildCat cat;
    private TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        double result = EMPTY_WEIGHT + this.cat.getWeight();
        if(this.next == null) {
            return result;
        }
        else {
            return result + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        double result = this.cat.computeMassIndex();
        if(this.next == null) {
            return result;
        }
        else {
            return result +  this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        if (this.next == null) {
            System.out.println(String.format("(%s)", this.cat.getName()));
        }
        else {
            System.out.print(String.format("(%s)--", this.cat.getName()));
            this.next.printCar();
        }
    }
}

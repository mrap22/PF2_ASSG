import java.util.*;

public class Main {
  static Scanner cin = new Scanner(System.in);
  static Animals temp;
  static Animals[][] animalCageArray = new Animals[5][];
  static CageArrange ca = new CageArrange();

  public static void main(String[] args) {

      String input;
      String numInput;



      System.out.println("Welcome to javari Park!");
      System.out.println("Input the number of animals");

      processAnimal("Cats", 0);
      processAnimal("Lion", 1);
      processAnimal("Eagle", 2);
      processAnimal("Parrots", 3);
      processAnimal("Hamsters", 4);

      System.out.println("Animals have been successfully recorded!\n");
      System.out.println("=============================================");
      System.out.println("Cage arrangement:");

      for(int i = 0; i < 5; i++) {
        if(animalCageArray[i] != null) {
            if(animalCageArray[i].length > 0)
                ca.cageArrange(animalCageArray[i]);
            }
        }

      boolean visit = true;

      while(visit) {
        visit = visitAnimal();
      }

  }

  static void processAnimal(String anim, int order) {

    if(anim.equals("Cats"))
      temp = new Cats();
    else if(anim.equals("Lion"))
      temp = new Lion();
    else if(anim.equals("Eagle"))
      temp = new Eagle();
    else if(anim.equals("Parrots"))
      temp = new Parrots();
    else
      temp = new Hamsters();

    System.out.printf("%s: ", temp.getSpesies());
    int numInput = Integer.parseInt(cin.nextLine());
    if(numInput > 0) {
      System.out.printf("Provide the information of %s(s)\n", temp.getSpesies());
      String input = cin.nextLine();
      processInput(input, numInput, temp, order);
    }
  }

  static void processInput(String input, int number, Animals animal, int order) {
    if(animal.getSpesies().equals("cat"))
      animalCageArray[order] = new Cats[number];
    else if(animal.getSpesies().equals("lion"))
      animalCageArray[order] = new Lion[number];
    else if(animal.getSpesies().equals("eagle"))
      animalCageArray[order] = new Eagle[number];
    else if(animal.getSpesies().equals("parrot"))
      animalCageArray[order] = new Parrots[number];
    else
      animalCageArray[order] = new Hamsters[number];

    String[] nameLength = input.split(",");
    for(int i = 0; i < nameLength.length; i++) {
      String[] detail = nameLength[i].split("\\|");
      if(animal.getSpesies().equals("cat"))
        animalCageArray[order][i] = new Cats(detail[0], Integer.parseInt(detail[1]));
      else if(animal.getSpesies().equals("lion"))
        animalCageArray[order][i] = new Lion(detail[0], Integer.parseInt(detail[1]));
      else if(animal.getSpesies().equals("eagle"))
        animalCageArray[order][i] = new Eagle(detail[0], Integer.parseInt(detail[1]));
      else if(animal.getSpesies().equals("parrot"))
        animalCageArray[order][i] = new Parrots(detail[0], Integer.parseInt(detail[1]));
      else
        animalCageArray[order][i] = new Hamsters(detail[0], Integer.parseInt(detail[1]));
    }
  }

  static boolean visitAnimal() {
    System.out.println("Which animal you want to visit?");
    System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
    int select = Integer.parseInt(cin.nextLine());

    if(select == 1)
      find(animalCageArray[0], "cat");
    else if (select ==5)
      find(animalCageArray[1], "lion");
    else if (select ==2)
      find(animalCageArray[2], "eagle");
    else if (select ==4)
      find(animalCageArray[3], "parrots");
    else if (select ==3)
      find(animalCageArray[4], "hamsters");
    else
      return false;

    System.out.println("Back to the office!\n");
    return true;
  }

  public static boolean find(Animals[] array, String anim) {

    if(anim.equals("cat"))
      temp = new Cats();
    else if(anim.equals("lion"))
      temp = new Lion();
    else if(anim.equals("eagle"))
      temp = new Eagle();
    else if(anim.equals("parrots"))
      temp = new Parrots();
    else
      temp = new Hamsters();

    boolean found = false;
    System.out.printf("Mention the name of %s you want to visit: ", temp.getSpesies());
    String nameInput = cin.nextLine();
    if(array != null) {
        for(int i = 0; i < array.length; i++) {
          if(array[i].getName().equals(nameInput)) {
            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n", nameInput, temp.getSpesies());
            System.out.println(temp.getOption());
            int opt = Integer.parseInt(cin.nextLine());
            array[i].interact(nameInput, opt);
            found = true;
          }
        }
    }
    if(found == false) {
      System.out.printf("There is no %s with that name! Back to the office!\n\n", anim);
    }
    return found;
  }


}

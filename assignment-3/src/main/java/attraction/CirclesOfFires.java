package attraction;

import java.util.*;

import javari.park.SelectedAttraction;
import javari.animal.Animal;
import Animals.*;

public class CirclesOfFires implements SelectedAttraction {
	
	private String name;
	private List<Animal> anims;
	private String type;
	
	public CirclesOfFires() {
		this.name = "Circle of Fires";
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getType() {
		return this.type;
	}
	
	public List<Animal> getPerformers() {
		return this.anims;
	}
	
	public boolean addPerformer(Animal performer) {
		if(performer.getType().equals("Lion") || performer.getType().equals("Whale")
				|| performer.getType().equals("Eagle")) {
			this.anims.add(performer);
			return true;
		}
		return false;
	}
	
}

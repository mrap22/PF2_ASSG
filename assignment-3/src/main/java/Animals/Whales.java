package Animals;

import Species.Mammals;
import javari.animal.Condition;
import javari.animal.Gender;

public class Whales extends Mammals {
    public Whales(int id, String type, String name, Gender gender, double length,
            double weight, Condition condition, String scon) {
    super(id, type, name, gender, length, weight, condition, scon);
    }
}
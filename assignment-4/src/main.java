import gui.FrontEnd;
import gui.BackEnd;
import javax.swing.*;

import java.awt.event.*;
import java.util.ArrayList;

public class main {
	
	static ImageIcon[] image = new ImageIcon[200];
	static ArrayList<String> cards;
	static ArrayList<String> matrix;
	static int pairs;
	
	public static void main(String[] args) {
		FrontEnd front = new FrontEnd();
		BackEnd back = new BackEnd();
		
		pairs = 18;
		
		image = back.getImages();
		cards = back.randomizeCard(pairs);
		matrix = back.randomizeMatrix(cards);
		
		front.mainFrame(image, matrix, pairs);
	}
}

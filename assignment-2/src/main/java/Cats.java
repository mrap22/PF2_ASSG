import java.util.Random;

public class Cats extends Animals{

  private static int number;
  String location = "indoor";
  String spesies = "cat";
  String option = "1: Brush the fur 2: Cuddle";

  public Cats(){
    super();
    this.cage = new Cages(length, this);
  }

  public String getSpesies() {
      return this.spesies;
  }

  public Cats(String name, int length) {
    super(name, length);
    this.cage = new Cages(length, this);
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getOption() {
    return this.option;
  }

  public void interact(String name, int opt) {
    super.interact(name, opt);
    Random rand = new Random();

    if(opt == 1) {
      System.out.printf("Time to clean %s's fur\n", name);
      System.out.printf("%s makes a voice: Nyaaan...\n", name);
    }

    else if(opt == 2) {
      String voice;
      int select = rand.nextInt(4);
      if(select == 0)
        voice = "Miaaaw..";
      else if(select == 1)
        voice = "Purrr..";
      else if(select == 2)
        voice = "Mwaw";
      else
        voice = "Mraaawr!";
      System.out.printf("%s makes a voice: %s\n", name, voice);
    }
    else
      System.out.println("You do nothing...");
  }

}

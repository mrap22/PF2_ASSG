package Animals;

import Species.Mammals;
import javari.animal.Condition;
import javari.animal.Gender;

public class Lion extends Mammals {
    public Lion(int id, String type, String name, Gender gender, double length,
            double weight, Condition condition, String scon) {
    super(id, type, name, gender, length, weight, condition, scon);
    }
}
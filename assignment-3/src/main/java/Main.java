import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import park.Park;
import attraction.*;
import Animals.*;
import javari.animal.Animal;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.lang.System;

import reader.FileCheck;
import reader.GenericReader;

public class Main {

    //static FileCheck fc = new FileCheck();

    static Scanner in = new Scanner(System.in);
    
    static Park park = new Park();

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String holder = "";
        Path[] csvs = new Path[3];
        List<String> attractionList = new ArrayList<String>();
        List<String> animalCategories = new ArrayList<String>();
        List<String> animalRecords = new ArrayList<String>();

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ... ");
        boolean error = true;
        try {
            File workingDir = new File(System.getProperty("user.dir"));
            csvs = FileCheck.check(workingDir.getAbsolutePath());
            error = false;
        }
        catch (IOException e) {
            System.out.print("File not found or incorrect file!\n");
        }

        while(error) {
            try{
                System.out.print("\nPlease provide the source data path: ");
                holder = in.nextLine();
                System.out.print("...Loading... ");
                csvs = FileCheck.check(holder.trim());
                System.out.println("Success... System is populating data...\n");
                System.out.printf("Found %d valid sections and %s invalid sections\n", FileCheck.getOK(), FileCheck.getBAD());
                error = false;
            }
            catch (IOException e) {
                System.out.println("File not found or incorrect file!\n");
            }
        }

        // Periksa valid attractions
        try {
	        GenericReader attract =  new GenericReader(csvs[0]);
	        attractionList = attract.getLines();
	        System.out.printf("Found %d valid attractions and %s invalid attraction\n", attract.countValidRecords(), attract.countInvalidRecords());
	
	        GenericReader categories = new GenericReader(csvs[1]);
	        animalCategories = categories.getLines();
	        System.out.printf("Found %d valid animal categories and %s invalid animal categories\n", categories.countValidRecords(), categories.countInvalidRecords());
	
	        GenericReader records = new GenericReader(csvs[2]);
	        animalRecords = records.getLines();
	        System.out.printf("Found %d valid animal records and %s animal records\n\n", records.countValidRecords(), records.countInvalidRecords());
	        setRecords(animalRecords);
	        
	        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
	        System.out.println("Please answer the question by typing the number. Type # if you want to return to the previous menu\n");
        }
        catch (IOException e) {
        	System.out.println("something went wrong");
        }
        
        
        
        int menuCode = 0;
        int selection = 0;
        String temp = "";
        while(menuCode != -1) {
        	manageLevel(menuCode);
        	try {
        		menuCode = Integer.parseInt(in.nextLine());
        	}
        	catch(Exception e) {
        		System.out.println("Command tidak dikenali");
        		continue;
        	}
        	if(menuCode == 1) { //aves
        		temp = in.nextLine();
        		if(temp.equals("#")) {
        			menuCode = 0;
        			manageLevel(0);
        			continue;
        		}
        		selection = Integer.parseInt(temp);
        		
        		if(selection == 1) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no eagle can perform any attraction, please choose other animals");
        			}
        		}
        		
        		else if(selection == 2) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no parrot can perform any attraction, please choose other animals");
        			}
        			
        		}
        	}
        	
        	else if(menuCode == 2) {
        		temp = in.nextLine();
        		if(temp.equals("#")) {
        			menuCode = 0;
        			manageLevel(0);
        			continue;
        		}
        		selection = Integer.parseInt(temp);
        		if(selection == 1) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
        			}
        		}
        		
        		else if(selection == 2) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no lion can perform any attraction, please choose other animals");
        			}
        		}
        		
        		else if(selection == 3) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no cat can perform any attraction, please choose other animals");
        			}
        		}
        		
        		else if(selection == 4) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no whale can perform any attraction, please choose other animals");
        			}
        		}
        		
        	}
        	else if(menuCode == 3) {
        		temp = in.nextLine();
        		if(temp.equals("#")) {
        			menuCode = 0;
        			manageLevel(0);
        			continue;
        		}
        		selection = Integer.parseInt(temp);
        		if(selection == 1) {
        			if(park.getEagles() != null) {
        				
        			}
        			else {
        				System.out.println("Unfortunately, no snake can perform any attraction, please choose other animals");
        			}
        		}
        		
        	}
        	else System.out.println("Command tidak dikenali");
        }
        
        
        in.close();
    }
    
    /*public setCategories();
    public setAttractions();*/
    public static void setRecords(List<String> arg) {
    	for(int i = 0; i < arg.size(); i++) {
    		park.addBinatang(arg.get(i));
    	}
    }
    
    
    public static int manageLevel(int arg) {
    	int ret = 0;
    	
    	if(arg == 0) {
    		ret = mainMenu();
    	}
    	else if(arg == 1) {
    		ret = gotoAves();
    	}
    	else if(arg == 2) {
    		ret = gotoAves();
    	}
    	else if(arg == 3) {
    		ret = gotoReptile();
    	}
    	else if(arg == -1) {
    		ret = -1;
    	}
    	return ret;
    }

    public static int mainMenu() {
        System.out.println("Javari Park has 3 sections:");
        System.out.println("1. Explore the Mammals");
        System.out.println("2. World of Aves");
        System.out.println("3. Reptilian Kingdom");
        System.out.println("Please choose your preferred section (type the number) : ");
        return 0;
    }

    public static int gotoAves() {
        System.out.println("--World of Aves--");
        System.out.println("1. Eagle");
        System.out.println("2. Parrot");
        System.out.println("Please choose your preferred animals (type the number) : ");
        return 1;
    }

    public static int gotoMammals() {
        System.out.println("--Explore the Mammals--");
        System.out.println("1. Hamster");
        System.out.println("2. Lion");
        System.out.println("3. Cat");
        System.out.println("4. Whale");
        System.out.println("Please choose your preferred animals (type the number) : ");
        return 2;
    }

    public static int gotoReptile() {
        System.out.println("--Reptilian Kingdom--");
        System.out.println("1. Snake");
        System.out.println("Please choose your preferred animals (type the number) : ");
        return 3;
    }
    
    public static boolean askPlayer(String atraksi, String hewan, String nama) {
    	System.out.print("Wow one more step,\nplease let us know your name: ");
    	String name = in.nextLine();
    	System.out.println("\nYeay, final check!");
    	System.out.println("Here is your final data, and the attraction you choose:");
    	System.out.printf("Name: %s\nAttractions: %s\nWith:%s\n\n", atraksi, hewan, nama);
    	System.out.print("Is the data correct? (Y/N): ");
    	String choose = in.nextLine();
    	if(choose.equalsIgnoreCase("Y")) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    
}

package park;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;
import Animals.*;
import attraction.*;

public class Park {
	
	private ArrayList<Animal> binatang;
	private List<Cats> cat;
    private List<Eagles> eagle;
    private List<Hamsters> hamster;
    private List<Lion> lion;
    private List<Parrots> parrot;
    private List<Snakes> snake;
    private List<Whales> whale;
	
	public Park() {
		 this.binatang = new ArrayList<Animal>();
		 this.cat = new ArrayList<Cats>();
		 this.eagle = new ArrayList<Eagles>();
		 this.hamster = new ArrayList<Hamsters>();
		 this.lion = new ArrayList<Lion>();
		 this.parrot = new ArrayList<Parrots>();
		 this.snake = new ArrayList<Snakes>();
		 this.whale = new ArrayList<Whales>();
	}
	
	public void setAttraction() {
		
	}
	
	public void addBinatang(String list) {
		List<String> arg = Arrays.asList(list.split(","));
		
		if(arg.get(2) == "Hamster") {
			hamster.add(new Hamsters(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
			
		}
		else if(arg.get(2) == "Cat") {
			cat.add(new Cats(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		else if(arg.get(2) == "Lion") {
			lion.add(new Lion(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		else if(arg.get(2) == "Whale") {
			whale.add(new Whales(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		else if(arg.get(2) == "Snake") {
			snake.add(new Snakes(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		else if(arg.get(2) == "Parrot") {
			parrot.add(new Parrots(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		else if(arg.get(2) == "Eagle") {
			eagle.add(new Eagles(Integer.parseInt(arg.get(0)), arg.get(1), arg.get(2), 
					Gender.parseGender(arg.get(3)),
					Double.parseDouble(arg.get(4)), Double.parseDouble(arg.get(5)),
					Condition.parseCondition(arg.get(7)), arg.get(6)));
		}
		
		
	}
	
	public List<Cats> getCats() {
		return this.cat;
	}
	
	public List<Eagles> getEagles() {
		return this.eagle;
	}
	
	public List<Hamsters> getHamsters() {
		return this.hamster;
	}
	
	public List<Lion> getLions() {
		return this.lion;
	}
	
	public List<Parrots> getParrots() {
		return this.parrot;
	}
	
	public List<Snakes> getSnake() {
		return this.snake;
	}
	
	public List<Whales> getWhales() {
		return this.whale;
	}
}

public class Lion extends Animals{
  static private int number;
  String location = "outdoor";
  static String spesies = "lion";
  String option = "1: See it hunting 2: Brush the mane 3: Disturb it";

  public Lion() {
    super();
    this.cage = new Cages(length, this);
  }

  public Lion(String name, int length) {
    super(name, length);
    this.cage = new Cages(length, this);
  }

  public String getSpesies() {
      return this.spesies;
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getOption() {
    return this.option;
  }

  public void interact(String name, int opt) {
    if(opt == 1) {
      System.out.println("Lion is hunting..");
      System.out.printf("%s makes a voice: err...!\n", name);
    }

    else if(opt == 2) {
      System.out.println("Clean the lion’s mane..");
      System.out.printf("%s makes a voice: Hauhhmm!\n", name);
    }
    else if(opt == 3) {
      System.out.printf("%s makes a voice: HAUHHMM!\n", name);
    }
    else
      System.out.println("You do nothing...");
  }


}

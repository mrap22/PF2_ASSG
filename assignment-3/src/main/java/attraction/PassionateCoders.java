package attraction;

import java.util.List;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

public class PassionateCoders implements SelectedAttraction{
	private String name;
	private List<Animal> anims;
	private String type;
	
	public PassionateCoders() {
		this.name = "Passionate Coders"	;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getType() {
		return this.type;
	}
	
	public List<Animal> getPerformers() {
		return this.anims;
	}
	
	public boolean addPerformer(Animal performer) {
		if(performer.getType().equals("Lion") || performer.getType().equals("Whale")
				|| performer.getType().equals("Eagle")) {
			this.anims.add(performer);
			return true;
		}
		return false;
	}
	
}

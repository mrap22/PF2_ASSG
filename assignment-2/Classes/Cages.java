public class Cages {
  private int length;
  private int height;
  private String type;

  public String getType() {
    return this.type;
  }

  public Cages(int length, Animals animal) {
    if(animal.getLocation().equals("indoor")) {

      if(animal.getLength() < 45) {
        this.length = 60;
        this.height = 60;
        this.type = "A";
      }

      else if(animal.getLength() <= 60 && animal.getLength() >= 45) {
        this.length = 60;
        this.height = 90;
        this.type = "B";
      }

      else {
        this.length = 60;
        this.height = 120;
        this.type = "C";
      }
    }

    else{
      if(animal.getLength() < 75) {
        this.length = 120;
        this.height = 120;
        this.type = "A";
      }

      else if(animal.getLength() <= 90 && animal.getLength() >= 75 ) {
        this.length = 120;
        this.height = 150;
        this.type = "B";
      }

      else {
        this.length = 120;
        this.height = 180;
        this.type = "C";
      }
    }
  }
}

public class Animals {
  protected String name;
  protected String option;
  protected int length;
  protected Cages cage;
  protected static String location;
  protected String spesies;

  public Animals() {
    this.name = "";
  }

  public Animals(String name, int length) {
    this.name = name;
    this.length = length;
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getName() {
    return this.name;
  }

  public String getCageType() {
    return this.cage.getType();
  }

  public String getSpesies() {
    return this.spesies;
  }

  public String getOption() {
    return this.option;
  }

  public void interact(String name, int opt) {
    return;
  }
}

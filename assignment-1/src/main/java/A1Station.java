import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
      
        TrainCar last = null;
        Scanner input = new Scanner(System.in);
        String[] command = new String[3];
        double massIndex = 0;
        int catCount = 0;
        int N = Integer.parseInt(input.nextLine());

        for(int i = 0; i < N; i++) {
            command = input.nextLine().split(",");
            if(last == null) {
                last = new TrainCar(new WildCat(command[0],Double.parseDouble(command[1]), Double.parseDouble(command[2])), null);
            }
            else {
                last = new TrainCar(new WildCat(command[0], Double.parseDouble(command[1]), Double.parseDouble(command[2])), last);
            }

            catCount++;

            if(last.computeTotalWeight() >= THRESHOLD) {
                massIndex += runTrain(last);
                last = null;
                printTrain(massIndex, catCount);
                massIndex = 0;
                catCount = 0;
            }

        }

        if(last != null) {
            massIndex += runTrain(last);
            printTrain(massIndex, catCount);
        }




    }

    static double runTrain(TrainCar car) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        car.printCar();
        return car.computeTotalMassIndex();
    }

    static void printTrain(double tmass, int numcat) {
      System.out.println(String.format("Average mass index of all cats: %.2f", (float) tmass / numcat));
      String status;
      int avmass = (int) tmass / numcat;
      if(avmass < 18.5) {
          status = "underweight";
      }
      else if(avmass >= 18.5 && avmass < 25.0) {
          status = "normal";
      }
      else if(avmass >= 25.0 && avmass < 30.0) {
          status = "overweight";
      }
      else {
          status = "obese";
      }
      System.out.println(String.format("In average, the cats in the train are *%s*", status));
    }
}

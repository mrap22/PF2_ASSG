import java.lang.*;
import java.util.ArrayList;

public class CageArrange {
  private Animals animal;

  String[] stringOfLevel = new String[3];

  ArrayList<Animals>[] levels = new ArrayList[3];
  int numOfAnimal;
  int modOfAnimal;
  Animals[] temp;

  int counter = 0;

  public ArrayList<Animals>[] cageArrange(Animals[] anim) {
    counter =  0;
    numOfAnimal = anim.length / 3;

    if(anim.length < 3) {
        numOfAnimal = (int) Math.ceil(anim.length / 3.0);
    }
    if(anim.length % 3 != 0) {
        modOfAnimal = anim.length - (2 * numOfAnimal);
    }
    else
        modOfAnimal = 0;

    // arrange based on first order

    levels[0] = new ArrayList<Animals>();
    for(int j = 0; j < numOfAnimal; j++) {

      if(counter < anim.length) {
          levels[0].add(anim[counter]);
          counter++;
        }
    }

    levels[1] = new ArrayList<Animals>();
    for(int j = 0; j < numOfAnimal; j++) {

      if(counter < anim.length) {
          levels[1 ].add(anim[counter]);
          counter++;
        }
    }

    levels[2] = new ArrayList<Animals>();
    for(int j = 0; j < numOfAnimal + modOfAnimal; j++) {

      if(counter < anim.length) {
          levels[2].add(anim[counter]);
          counter++;
        }
    }

    stringOfLevel = setStringList(levels);

    System.out.printf("location: %s\n", anim[0].getLocation());
    System.out.printf("level 3: %s\n", stringOfLevel[2]);
    System.out.printf("level 2: %s\n", stringOfLevel[1]);
    System.out.printf("level 1: %s\n\n", stringOfLevel[0]);

    //modify order

    ArrayList<Animals> tempA = new ArrayList<Animals>();

    for(int i = 0; i < levels[0].size(); i++) {
        tempA.add(levels[0].get(i));
        System.out.println(tempA.get(i).getName());
    }
    levels[0].clear();
    for(int i = 0; i < levels[2].size(); i++) {
        levels[0].add(levels[2].get(i));
    }
    levels[2].clear();
    for(int i = 0; i < levels[1].size(); i++) {
        levels[2].add(levels[1].get(i));
    }
    levels[1].clear();
    for(int i = 0; i < tempA.size(); i++) {
        levels[1].add(tempA.get(i));
    }

    for(int i = 0; i < 3; i++) {

        tempA.clear();
        if(levels[i].size() != 0) {
            for(int j = 0; j < levels[i].size(); j++) {
                tempA.add(levels[i].get(j));
            }
            levels[i].clear();
            for(int j = 0; j < tempA.size(); j++) {
                levels[i].add(tempA.get(tempA.size() - 1 - j));
            }
        }
    }

    stringOfLevel = setStringList(levels);

    System.out.println("After rearrangement...");
    System.out.printf("level 3: %s\n", stringOfLevel[2]);
    System.out.printf("level 2: %s\n", stringOfLevel[1]);
    System.out.printf("level 1: %s\n\n", stringOfLevel[0]);

    return levels;
  }

  private String[] setStringList(ArrayList<Animals>[] mult) {
    String[] temp = new String[3];

    temp[0] = "";
    for(int i = 0; i < levels[0].size(); i++) {
        if(levels[0].size() != 0)
            temp[0] += String.format("%s (%d - %s), ", levels[0].get(i).getName(), levels[0].get(i).getLength(), levels[0].get(i).getCageType());
        else
            temp[0] = "";
    }
    temp[1] = "";
    for(int i = 0; i < levels[1].size(); i++) {
        if(levels[1].size() != 0)
            temp[1] += String.format("%s (%d - %s), ", levels[1].get(i).getName(), levels[1].get(i).getLength(), levels[1].get(i).getCageType());
        else
            temp[1] = "";
    }
    temp[2] = "";
    for(int i = 0; i < levels[2].size(); i++) {
        if(levels[2].size() != 0)
            temp[2] += String.format("%s (%d - %s), ", levels[2].get(i).getName(), levels[2].get(i).getLength(), levels[2].get(i).getCageType());
        else
            temp[0] = "";
    }

    return temp;
  }


}

package reader;

import java.io.IOException;
import java.nio.file.Path;

import javari.reader.CsvReader;

public class GenericReader extends CsvReader {

    public GenericReader(Path files) throws IOException {
        super(files);
    }

    public long countValidRecords() {

        long ctr = 0;

        for(int i = 0; i < this.lines.size(); i++) {
            if(!lines.get(i).isEmpty()) {
                ctr++;
            }
        }
        return ctr;
    }

    public long countInvalidRecords() {

        long ctr = 0;

        for(int i = 0; i < this.lines.size(); i++) {
            if(lines.get(i).isEmpty()) {
                ctr++;
            }
        }
        return ctr;
    }
}

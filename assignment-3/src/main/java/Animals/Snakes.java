package Animals;

import Species.Reptiles;
import javari.animal.Condition;
import javari.animal.Gender;

public class Snakes extends Reptiles {
    public Snakes(int id, String type, String name, Gender gender, double length,
            double weight, Condition condition, String scon) {
    super(id, type, name, gender, length, weight, condition, scon);
    }
}

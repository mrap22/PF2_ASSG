package Species;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public class Mammals extends Animal {
	
	private boolean specCondition;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String specC) {
        super(id, type, name, gender, length, weight, condition);
        if(specC != "") 
        	this.specCondition = true;   
        else
        	this.specCondition = false;
    }
    
    public boolean specificCondition() {
    	return this.specCondition;
    }
}

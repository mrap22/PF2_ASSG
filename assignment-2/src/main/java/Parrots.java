import java.util.*;

public class Parrots extends Animals{
  Scanner in = new Scanner(System.in);
  static private int number;
  String location = "indoor";
  static String spesies = "parrot";
  String option = "1: Order to fly 2: Do conversation";

  public Parrots() {
    super();
  }
  public Parrots(String name, int length) {
    super(name, length);
  }

  public String getSpesies() {
      return this.spesies;
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getOption() {
    return this.option;
  }

  public void interact(String name, int opt) {
    if(opt == 1){
      System.out.printf("Parrot %s Flies!\n", name);
      System.out.printf("%s makes a voice: FLYYYY…..\n", name);
    }
    else if(opt == 2) {
      System.out.print("You say: ");
      String temp = in.nextLine();
      System.out.printf("%s says: %s", name, temp.toUpperCase());
    }
    else
      System.out.printf("%s says: HM\n?", name);
    }
}

public class Hamsters extends Animals{

  static private int number;
  String location = "indoor";
  static String spesies = "hamster";
  String option = "1: See it gnawing 2: Order to run in the hamster wheel";

  public Hamsters() {
    super();
    this.cage = new Cages(length, this);
  }
  public Hamsters(String name, int length) {
    super(name, length);
    this.cage = new Cages(length, this);
  }

  public String getSpesies() {
      return this.spesies;
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getOption() {
    return this.option;
  }

  public void interact(String name, int opt) {
    if(opt == 1)
      System.out.printf("%s makes a voice: ngkkrit.. ngkkrrriiit\n", name);
    else if(opt == 2)
      System.out.printf("%s makes a voice: trrr... trrr...\n", name);
    else
      System.out.println("You do nothing...");
  }

}

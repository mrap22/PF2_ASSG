package reader;

import java.io.File;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Arrays;


public class FileCheck {

    private static List<String> bs = Arrays.asList(
    "animals_attractions.csv",
    "animals_categories.csv",
    "animals_records.csv"
    );


    private static Path[] gpath = new Path[3];

    public static Path[] check(String path) throws IOException{
        File folder = new File(path);
        File[] list = folder.listFiles();
        
        if(list != null) {
	        for(int i = 0; i < list.length; i++) { 
	            if(list[i].canRead() && bs.get(0).equals(list[i].getName())) {
	                gpath[0] = Paths.get(list[i].getAbsolutePath());
	            }
	
	            else if(list[i].canRead() && bs.get(1).equals(list[i].getName())) {
	                gpath[1] = Paths.get(list[i].getAbsolutePath());
	            }
	
	            else if(list[i].canRead() && bs.get(2).equals(list[i].getName())) {
	                gpath[2] = Paths.get(list[i].getAbsolutePath());
	            }
	        }
        }
        if(gpath[0] == null || gpath[1] == null || gpath[2] == null) {
        	throw new IOException("failed");
        }

        return gpath;
    }

    public static int getOK() {
        int temp = 0;
        for(int i = 0; i < 3; i++) {
            if(gpath[i] != null) {
                temp++;
            }
        }
        return temp;
    }

    public static int getBAD() {
        int temp = 0;
        for(int i = 0; i < 3; i++) {
            if(gpath[i] == null) {
                temp++;
            }
        }
        return temp;
    }


}

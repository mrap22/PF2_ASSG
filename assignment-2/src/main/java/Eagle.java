public class Eagle extends Animals{

  static private int number;
  String location = "outdoor";
  static String spesies = "eagle";
  String option = "1: Order to fly";

  public Eagle() {
    super();
    this.cage = new Cages(length, this);
  }

  public String getSpesies() {
      return this.spesies;
  }

  public String getLocation() {
    return this.location;
  }

  public int getLength() {
    return this.length;
  }

  public String getOption() {
    return this.option;
  }

  public Eagle(String name, int length) {
    super(name, length);
    this.cage = new Cages(length, this);
  }

  public void interact(String name, int opt) {
    if(opt == 1) {
      System.out.printf("%s makes a voice: kwaakk…\n", name);
      System.out.println("You hurt!");
    }
    else
      System.out.println("You do nothing...");
  }
}
